import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostModule } from './post/post.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'b7vgl6excizcdxewl8ye-mysql.services.clever-cloud.com',
      port: 3306,
      username: 'urfjzyg53jw2ehtq',
      password: '6XU93bFwuyp1fvJZyupG',
      database: 'b7vgl6excizcdxewl8ye',
      entities: [__dirname + './**/*entity{.ts,.js}'],
      autoLoadEntities: true,
      synchronize: true,
    }),
    PostModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
