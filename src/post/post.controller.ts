import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { CreatePostDto, EditPostDto } from './dto';
import { PostService } from './post.service';

@Controller('post')
export class PostController {

    constructor(private readonly postService: PostService){}

    @Get()
    async getMany(){
        const data =  await this.postService.getMany()
        return {
            message: "OK",
            data
        }
    }

    @Get(':id')
    async getOne( @Param('id', ParseIntPipe) id : number){
        const data =  await this.postService.getOne(id);
        return {
            message: "OK",
            data
        }
    }

    @Post('create')
    async createOne( @Body() dto: CreatePostDto ){
        const data = await this.postService.creteOne(dto)
        return {
            message: "Created",
            data
        }
    }

    @Put('update/:id')
    async editOne( @Param('id', ParseIntPipe) id : number, @Body() dto: EditPostDto ){
        const data =  await this.postService.editOne(id, dto);
        return {
            message: "Edited",
            data
        }
    }

    @Delete('delete/:id')
    async deleteOne( @Param('id', ParseIntPipe) id : number ){
        const data = await this.postService.deleteOne(id);
        return {
            message: "Deleted",
            data
        }
    }
    
}
