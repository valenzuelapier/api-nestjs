import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePostDto, EditPostDto } from './dto';
import { Post } from './entities';

@Injectable()
export class PostService {

    constructor(
        @InjectRepository(Post)
        private readonly postRepository: Repository<Post>
    ){}
        
    async getMany(): Promise<Post[]>{
        return await this.postRepository.find();
    }

    async getOne(id:number): Promise<Post>{
        const data = await this.postRepository.findOne(id);
        if(!data) throw new NotFoundException();
        return data;
    }

    async creteOne(dto: CreatePostDto): Promise<any>{
        const data = this.postRepository.create(dto as any);
        return await this.postRepository.save(data)
    }

    async editOne(id:number, dto: EditPostDto): Promise<any>{
        const data = await this.postRepository.findOne(id);
        if(!data) throw new NotFoundException();

        const dataEdit = Object.assign(data, dto);
        return await this.postRepository.save(dataEdit);
    }

    async deleteOne(id:number): Promise<any>{
        return await this.postRepository.delete(id);
    }


}
