import { CreatePostDto } from ".";
import { PartialType, OmitType } from "@nestjs/mapped-types";

export class EditPostDto extends PartialType(OmitType(CreatePostDto, ['slug'])){}